import random

from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views    import APIView

from .models      import Product
from .models      import User
from .serializers import ProductSerializer
from .producer    import publish

class ProductViewSet(viewsets.ViewSet):
    """
    # List products
    > GET /api/products
    """
    def list(self, request):
        products   = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)

    """
    # Create new product
    > POST /api/products
    """
    def create(self, request):
        serializer = ProductSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        publish('product.created', serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    """
    # Destroy product
    > DELETE /api/products/:id
    """
    def destroy(self, request, id=None):
        product = Product.objects.get(id=id)
        product.delete()
        publish('product.destroyed', id)
        return Response(status=status.HTTP_204_NO_CONTENT)


    """
    # Update product
    > PATCH /api/products/:id
    """
    def update(self, request, id=None):
        product = Product.objects.get(id=id)
        serializer = ProductSerializer(instance=product, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        publish('product.updated', serializer.data)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    """
    # Retrieve product
    > GET /api/products/:id
    """
    def show(self, request, id=None):
        product = Product.objects.get(id=id)
        serializer = ProductSerializer(product)
        return Response(serializer.data)


class UserAPIView(APIView):
    def get(self, _):
        users = User.objects.all()
        user  = random.choice(users)
        return Response({
            'id': user.id
        })
