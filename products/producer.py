RABBITMQ_URL="amqps://zesrtyum:8P1YOW7XrwL_1ayHkWl2v9Mwq4mvlDi1@jackal.rmq.cloudamqp.com/zesrtyum"


import os
import sys
import logging
import json
import pika

from dotenv import load_dotenv
load_dotenv()

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)

params     = pika.URLParameters(os.environ.get('RABBITMQ_URL'))
connection = pika.BlockingConnection(params)
channel    = connection.channel()


def publish(method, body, queue="app"):
    properties = pika.BasicProperties(method)
    channel.basic_publish(
        exchange='',
        routing_key=queue,
        body=json.dumps(body),
        properties=properties
    )
    logger.info("Published msg from::admin, to:"+queue+", props: "+str(properties)+", body: "+json.dumps(body))
