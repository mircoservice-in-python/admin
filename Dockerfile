FROM python:3.9

ENV PYTHONUNBUFFERED 1
RUN mkdir -p /var/www/admin
WORKDIR /var/www/admin

COPY requirements.txt /var/www/admin/requirements.txt

RUN pip install -r requirements.txt

COPY . /var/www/admin
