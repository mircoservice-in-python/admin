import os
import logging
import pika
from dotenv import load_dotenv
load_dotenv()
logger = logging.getLogger(__name__)

QUEUE_NAME = "admin"

params     = pika.URLParameters(os.environ.get('RABBITMQ_URL'))
connection = pika.BlockingConnection(params)
channel    = connection.channel()

channel.queue_declare(queue=QUEUE_NAME)

def callback(channel, method, properties, body):
    logger.info("Received in queue: "+ QUEUE_NAME)
    print(body)


print("Started consuming messges from queue "+QUEUE_NAME+"...")

channel.basic_consume(queue=QUEUE_NAME, on_message_callback=callback, auto_ack=True)
channel.start_consuming()
channel.close()
