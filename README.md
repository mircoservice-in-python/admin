# Admin Service

## Run development command

Some commands run better into container image, example to run migrate:

```sh
:~$ docker-compose exec backend sh
~# python manage.py migrate
```


## Run development server

```sh
python3 manage.py runserver
```
## Database migration

```sh
python manage.py makemigrations && python manage.py migrate
```
